﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AISimulation.JarvisClasses
{
    public class Jarvis
    {
        //public properties 
        public JarvisFacts Facts { get; set; }
        public JarvisHelpMe HelpMe { get; set; }
        public JarvisJokes Jokes { get; set; }
        public JarvisMath Math { get; set; }


        //constructor
        public Jarvis()
        {
            Facts = new JarvisFacts();
            HelpMe = new JarvisHelpMe();
            Jokes = new JarvisJokes();
            Math = new JarvisMath();

        }

        public virtual string GetWords()
        {
            return "words";
        }


    }
}
