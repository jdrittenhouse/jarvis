﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AISimulation.JarvisClasses
{
    public class JarvisMath
    {
        public int Age { get; set; }
        public int Years { get; set; }
        public int Days { get; set; }   
        public String Month { get; set; }


        public string GetExactAge(DateTime birthday)
        {
            DateTime today = DateTime.Today;

            int months = today.Month - birthday.Month;
            int years = today.Year - birthday.Year;

            if (today.Day < birthday.Day)
            {
                months--;
            }

            if (months < 0)
            {
                years--;
                months += 12;
            }

            int days = (today - birthday.AddMonths((years * 12) + months)).Days;

            return string.Format("{0} year{1}, {2} month{3} and {4} day{5}",
                                 years, (years == 1) ? "" : "s",
                                 months, (months == 1) ? "" : "s",
                                 days, (days == 1) ? "" : "s");
        }

    }
    }

