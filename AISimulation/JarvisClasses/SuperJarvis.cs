﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AISimulation.JarvisClasses
{
    public class SuperJarvis : Jarvis
    {
        public int numberOne { get; set; }
        public override string GetWords()
        {
            return "other words";
        }
    }
}
