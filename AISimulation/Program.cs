﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AISimulation.JarvisClasses;
using System.IO;

namespace AISimulation
{
    class Program
    {
        static void Main(string[] args)
        {
            Jarvis Jarvis = new Jarvis();
            //var myBirthDay = new DateTime(1994, 5, 6);
            Console.WriteLine("Hello, I'm Jarvis. Who are you?");
            var name = Console.ReadLine();

            Console.WriteLine("Hello " + name +". What can I help you with?");
            var helpRequest = Console.ReadLine();
            while(helpRequest.ToLower().Trim() != "i quit")
            {
                if (helpRequest.ToLower().Trim() == "help")
                {
                    Jarvis.HelpMe.ListCommands();
                }
                else if(helpRequest.Contains("how old "))
                {

                }
                Console.WriteLine("Anything else I can help with?");
                helpRequest = Console.ReadLine();

           
            }
           

            Console.WriteLine("Have a nice day");
        }
    }
}
